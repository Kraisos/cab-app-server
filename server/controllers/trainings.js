const Training = require("../models/training");

/**
 * @swagger
 *
 * components:
 *   schemas:
 *     Training:
 *       type: object
 *       properties:
 *         id:
 *           type: Number
 *         duration:
 *           type: Number
 */
let controller = {
	getById: async (id, ctx, next) => {
		try {
			ctx.training = await Training.findById(id)
				.populate("users")
				.exec();
			return next();
		} catch (error) {
			ctx.status = 404;
		}
	},

	create: async ctx => {
		let training = new Training(ctx.request.body);

		training = await training.save();

		ctx.body = training;
		ctx.status = 201;
	},

	/**
	 * @swagger
	 *
	 * /trainings/{training_id}:
	 *   get:
	 *     summary: get a training by id
	 *     operationId: readTraining
	 *     tags:
	 *       - training
	 *     parameters:
	 *       - name: training_id
	 *         in: path
	 *         required: true
	 *         description: the id of the training to retrieve
	 *         schema:
	 *           type: string
	 *     responses:
	 *       '200':
	 *         description: success
	 *         content:
	 *           application/json:
	 *             schema:
	 *               $ref: '#/components/schemas/Training'
	 *       '404':
	 *         description: User not found
	 *
	 */
	read: ctx => {
		ctx.body = ctx.training;
	},

	update: async ctx => {
		ctx.training.date = Date.now();
		training = await ctx.training.save();
		ctx.body = `WIP: it currently just changes the date to now()\n${training}`;
		// TODO: implement update function
	},

	delete: async ctx => {
		if (!ctx.training) {
			ctx.status = 404;
			return;
		}
		await Training.findByIdAndDelete(ctx.training._id).exec();
		ctx.status = 204;
	},

	list: async ctx => {
		let trainings = await Training.find({}).exec();
		ctx.body = trainings;
	},

	deleteAll: async ctx => {
		await Training.deleteMany({}).exec();
		ctx.status = 204;
	},

	readUsers: async ctx => {
		switch (ctx.query.role) {
			case "athlete":
				ctx.body = ctx.training.users.filter(user => user.role == "Athlete");
				break;

			case "trainer":
				ctx.body = ctx.training.users.filter(user => user.role == "Trainer");
				break;

			default:
				ctx.body = ctx.training.users;
				break;
		}
	}
};

module.exports = controller;

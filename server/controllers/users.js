const Users = require("../models/users");
const Trainings = require("../models/training");

/**
 * @swagger
 *
 * components:
 *   schemas:
 *     User:
 *       type: object
 *       properties:
 *         id:
 *           type: Number
 *         email:
 *           type: string
 *     UserPartial:
 *       type: object
 *       properties:
 *         email:
 *           type: string
 *         password:
 *           type: string
 *       required:
 *         - email
 *         - password
 *     UsersArray:
 *       type: array
 *       items:
 *         $ref: '#/components/schemas/User'
 */
let controller = {
	getById: async (id, ctx, next) => {
		try {
			ctx.user = await Users.User.findById(id).exec();
			return next();
		} catch (error) {
			ctx.status = 404;
		}
	},

	create: async ctx => {
		try {
			switch (ctx.query.role) {
				case "trainer":
					let trainer = new Users.Trainer(ctx.request.body);

					trainer = await trainer.save();

					ctx.body = trainer;
					ctx.status = 201;
					break;

				case "athlete":
					let athlete = new Users.Athlete(ctx.request.body);

					athlete = await athlete.save();

					ctx.body = athlete;
					ctx.status = 201;
					break;

				default:
					ctx.body = "You must specify a role";
					ctx.status = 400;
					break;
			}
		} catch (error) {
			console.error(error);
			ctx.status = 400;
		}
	},

	read: ctx => {
		ctx.body = ctx.user;
	},

	update: ctx => {
		ctx.body = `WIP: Update user with id ${ctx.user._id}`;
		// TODO: implement update function
	},

	delete: async ctx => {
		if (!ctx.user) {
			ctx.status = 404;
			return;
		}
		await Users.User.findByIdAndDelete(ctx.user._id).exec();
		ctx.status = 204;
	},

	list: async ctx => {
		let users = [];
		switch (ctx.query.role) {
			case "trainer":
				users = await Users.Trainer.find({}).exec();
				break;

			case "athlete":
				users = await Users.Athlete.find({}).exec();
				break;

			default:
				users = await Users.User.find({}).exec();
		}
		ctx.body = users;
	},

	deleteAll: async ctx => {
		await Users.User.deleteMany({}).exec();
		ctx.status = 204;
	},

	readTrainings: async ctx => {
		let trainings = await Trainings.find({ users: ctx.user._id }).exec();
		ctx.body = trainings;
	}
};

module.exports = controller;

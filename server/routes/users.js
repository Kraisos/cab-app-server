const users = require("../controllers/users");

module.exports = router => {
	router
		// This will put the right user in 'ctx.user' each time
		// a route with the paramater ':user_id' is invoked
		.param("user_id", users.getById)

		// CRUD operations
		.post("/users", users.create)
		.get("/users/:user_id", users.read)
		.put("/users/:user_id", users.update)
		.delete("/users/:user_id", users.delete)

		// Get a list of all users
		.get("/users", users.list)

		// Delete all users
		.delete("/users", users.deleteAll)

		// Get the traingins from that user
		.get("/users/:user_id/trainings", users.readTrainings);
};

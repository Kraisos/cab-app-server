const trainings = require("../controllers/trainings");

module.exports = router => {
	router
		// This will put the right user in 'ctx.user' each time
		// a route with the paramater ':user_id' is invoked
		.param("training_id", trainings.getById)

		// CRUD operations
		.post("/trainings", trainings.create)
		.get("/trainings/:training_id", trainings.read)
		.put("/trainings/:training_id", trainings.update)
		.delete("/trainings/:training_id", trainings.delete)

		// Get a list of all trainings
		.get("/trainings", trainings.list)

		// Delete all trainings
		.delete("/trainings", trainings.deleteAll)

		// Get all the users from that training
		.get("/trainings/:training_id/users", trainings.readUsers);
};

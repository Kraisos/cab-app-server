module.exports = router => {
	require("./users")(router);
	require("./trainings")(router);
};

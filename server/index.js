// koa
const Koa = require("koa");
const cors = require("@koa/cors");
const Bodyparser = require("koa-bodyparser");
const Router = require("koa-router");
const Routes = require("./routes/routes");
const serve = require("koa-static");

// mongodb
const Mongoose = require("mongoose");

// swagger
const Swagger = require("./middlewares/swagger");
const SwaggerUi = require("koa2-swagger-ui");

const uri = "mongodb+srv://cab-app-user:e375Wt8xNE93vBt@cluster0-pyqws.gcp.mongodb.net/cab-app?retryWrites=true&w=majority";
const local_uri = "mongodb://localhost:27017/cab-app";
// Connect to the MongoDB database (options to avoid warnings)
Mongoose.connect(uri, {
	useNewUrlParser: true
});
var db = Mongoose.connection;
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", function() {
	console.log("Connection to the database successfull !");
});

const app = new Koa();
const router = new Router();

// The api will be available at <domain>/api/...
router.prefix("/api");

// Create the routes
Routes(router);

// Options to generate the swagger documentation
const swaggerOptions = {
	definition: {
		openapi: "3.0.0",
		info: {
			title: "Cab App",
			version: "1.0.0",
			description: "description TODO"
		}
	},
	/**
	 * Paths to the API docs. The library will fetch comments marked
	 * by a @swagger tag to create the swagger.json document
	 */
	apis: [
		//'server/controllers/auth.js',
		//"server/controllers/users.js",
		"server/controllers/*.js"
	],
	// where to publish the document
	path: "/swagger.json"
};

// Call our own middleware (see in file)
const swagger = Swagger(swaggerOptions);

// Build the UI for swagger and expose it on the /doc endpoint
const swaggerUi = SwaggerUi({
	routePrefix: "/api/doc",
	swaggerOptions: {
		url: swaggerOptions.path
	}
});

app.use(swagger)
	.use(swaggerUi)

	.use(Bodyparser())

	.use(cors())

	// Use our routes
	.use(router.routes())
	.use(router.allowedMethods())

	// Use static files for the frontend in the public folder
	.use(serve(__dirname + "/public"))

	// listen
	.listen(process.env.PORT);

console.log(`Server listening on port ${process.env.PORT}`);

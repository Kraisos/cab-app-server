const Mongoose = require("mongoose");
const Schema = Mongoose.Schema;

const trainingSchema = new Mongoose.Schema(
	{
		date: {
			type: Date,
			required: true
		},
		duration: {
			type: Number,
			required: true
		},
		users: {
			type: [
				{
					type: Schema.Types.ObjectId,
					ref: "User"
				}
			]
		}
	},
	{ timestamps: true }
);

const Training = Mongoose.model("Training", trainingSchema);

module.exports = Training;

const Mongoose = require("mongoose");

// Base User Schema
const userSchema = new Mongoose.Schema(
  {
    firstName: {
      type: String,
      required: true
    },
    lastName: {
      type: String,
      required: true
    },
    email: {
      type: String
    }
  },
  {
    timestamps: true,
    discriminatorKey: "role"
  }
);

const User = Mongoose.model("User", userSchema);

// A Trainer can actually login, therefore it has a username and password
const Trainer = User.discriminator(
  "Trainer",
  new Mongoose.Schema({
    username: {
      type: String,
      required: true
    },
    password: {
      type: String,
      required: true
    }
  })
);

// An Athlete belongs to a group: 'sprint', 'demi-fond' or 'multiple'
const Athlete = User.discriminator(
  "Athlete",
  new Mongoose.Schema({
    group: {
      type: String,
      required: true,

      // TODO replace this with a ref to a group document ?
      // In this case maybe don't even ref the group here, but ref the athletes in the group doc ?
      enum: ["sprint", "demi-fond", "multiple"]
    }
  })
);

// Export only Trainer and Athlete, as 'User' is only there for some base properties
module.exports = { User, Trainer, Athlete };

# Server

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run dev
```

### Compiles and minifies for production

```
npm run build
```

### .env

create a file named ".env" at the root of your project containing the line

```
PORT=3000
```

You can choose the port that the server will listen on
